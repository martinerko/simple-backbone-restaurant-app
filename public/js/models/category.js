var CategoryModel = Backbone.Model.extend({
	urlRoot: '/category',
	defaults: {
		category: '',
		items: []
	}
});