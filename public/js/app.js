var AppRouter = Backbone.Router.extend({
	routes: {
		"": "list",
		"menu-items/new": "itemForm",
		"menu-items/:item": "itemDetails",
		"category/:category_name": "categoryDetails",
		"orders": "orders"
	},

	initialize: function  () {
		//create one menu item view that we will reuse
		this.menuItemModel = new MenuItemModel();
		this.menuItemView = new MenuItemView(
			{
				model: this.menuItemModel
			}
		);

		this.menuItemsCollection = new MenuItemsCollection();
		this.menuItemsCollection.fetch();

		this.menuView = new MenuView(
			{
				collection: this.menuItemsCollection
			}
		);

		this.categoryModel = new CategoryModel();
		this.categoryView = new CategoryView(
			{
				model: this.categoryModel
			}
		);

		this.ordersMenuItemsCollection = new MenuItemsCollection();
		this.ordersView = new OrdersView(
			{
				collection: this.ordersMenuItemsCollection
			}
		);

		this.menuItemFormView = new MenuItemFormView(
			{
				model: this.menuItemModel
			}
		);
	},

	list: function () {
		//this.menuModel.fetch();
		$('#app').html(this.menuView.render().el);
	},

	itemDetails: function (item) {
		/*
		var menuItemModel = this.menuItemModel;
		menuItemModel.set('id', item);
		menuItemModel.fetch();
		*/
		this.menuItemView.model = this.menuItemsCollection.get(item);
		$('#app').html(this.menuItemView.render().el);
	},

	itemForm: function () {
		$('#app').html(this.menuItemFormView.render().el);
	},

	categoryDetails: function (category_name) {
		var categoryModel = this.categoryModel;
		categoryModel.set('id', category_name);
		categoryModel.fetch();
		$('#app').html(this.categoryView.render().el);
	},

	orders: function(){
		$('#app').html(this.ordersView.render().el);
	}
});

var app = new AppRouter();

$(function() {
	Backbone.history.start();
});