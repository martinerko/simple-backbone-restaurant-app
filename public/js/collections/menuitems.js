var MenuItemsCollection = Backbone.Collection.extend({
	url: '/items',
	model: MenuItemModel,
	comparator: 'name' //sort by name
});