var MenuItemView = Backbone.View.extend({
	template: Handlebars.compile(
		'<div>' +
		'<h1>{{name}}</h1>' +
		'<p><a href="#/category/{{category}}" title="more from {{category}}"><span class="label">{{category}}</span></a></p>' +
		'<img src="photos/{{imagepath}}" class="img-polaroid" alt="{{name}}" />' +
		'<div><button type="button" class="btn btn-primary" data-id="{{id}}">Order</button></div>' +
		'</div>'
	),

	initialize: function(){
		this.listenTo(this.model, "change", this.render);
	},

	render: function () {
		this.$el.html(this.template(this.model.attributes));
		this.delegateEvents({
			'click .btn-primary': 'addToOrder'
		});
		return this;
	},

	addToOrder: function(e) {
		var model = app.menuItemsCollection.get($(e.currentTarget).attr("data-id"));
		app.ordersMenuItemsCollection.add(model);
		app.navigate('orders', {trigger: true});
	}
});