var CategoryView = Backbone.View.extend({
	template: Handlebars.compile(
		'<div>' +
		'<h1>{{category}}</h1>' +
		'<ul>' + 
		'{{#each items}}<a href="#/menu-items/{{this.url}}" title="{{this.name}}"><img src="/photos/{{this.imagepath}}" alt="{{this.name}}" class="img-polaroid" /></a></li>{{/each}}' +
		'</ul>' +
		'</div>'
	),

	initialize: function(){
		this.listenTo(this.model, "change", this.render);
	},

	render: function () {
		this.$el.html(this.template(this.model.attributes));
		return this;
	}
});