var OrdersView = Backbone.View.extend({
	template: Handlebars.compile(
		'<h1>Ordered items</h1>' +
		'{{#each models}}' +
		'<img src="photos/{{attributes.imagepath}}" class="img-polaroid" />' +
		'<div><button type="button" class="btn btn-danger" data-id="{{attributes.id}}">remove</button></div>' +
		'{{/each}}'
	),

	initialize: function(){
		this.listenTo(this.collection, "remove", this.render);
	},

	render: function () {
		this.$el.html(this.template(this.collection));
		this.delegateEvents({
			'click .btn-danger': 'remove'
		});
		return this;
	},

	remove: function(e){
		var model = this.collection.get($(e.currentTarget).attr("data-id"));
		this.collection.remove(model);
	}

});